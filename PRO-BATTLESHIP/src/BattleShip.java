import java.util.Scanner;
public class BattleShip {
		
	 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s;
		char c;
		int col;
		Board board=new Board();
		board.printBoard(true);
		Scanner input = new Scanner(System.in);
		boolean exit=false;
		while (!exit){
			System.out.println("SHOTS: "+board.getNumShots());
			System.out.println("SUNK SHIPS: "+board.getNumSunkShips());
			board.printBoard(true);
			System.out.println("Enter Row(letter) ;");
			s=input.next();
			c=s.charAt(0);
			System.out.println("Enter Column:");
			col=input.nextInt();
			int result =board.shootAt(c,col);
			switch(result){
			case Board.RESULT_ERROR:
				System.out.println("ERROR");
				break;
			case Board.RESULT_SUNK:
				System.out.println("SUNK");
				break;
			case Board.RESULT_WATER:
				System.out.println("WATER");
				break;
			case Board.RESULT_RESUNK:
				System.out.println("THAT SHIP WAS ALREADY SUNK");
			}
			
		}
	}

}
