
public class Board {
	private static final char EMPTY='�';
	private static final char SHIP='+';
	private static final char WATER='0';
	private static final char SUNK='X';
	
	private static final int NUM_ROWS=8;
	private static final int NUM_COLS=8;
	
	private static final int NUM_SHIPS=10;
	
	private char[][]board= new char[NUM_ROWS][NUM_COLS] ;
	
	public static final int RESULT_WATER=0;
	public static final int RESULT_SUNK=1;
	public static final int RESULT_RESUNK=2;
	public static final int RESULT_ERROR=3;
	private int numShots=0;
	private int numSunkShips;
	
	public Board(){
		resetBoard();
		putShipsRandomly();
	}
	private void resetBoard(){
		for (int i=0;i<NUM_ROWS;i++){
			for(int j=0;j<NUM_COLS;j++){
				board[i][j]=EMPTY;	
			}
		}
	numShots=0;
	numSunkShips=0;
	}
	private void putShipsRandomly(){
		int count=0                                                                                                                       ;
		int randomRow, randomColumn;
		while (count<NUM_SHIPS){
			randomRow=(int)(Math.random()*NUM_ROWS);
			randomColumn=(int)(Math.random()*NUM_COLS);
			if (board[randomRow][randomColumn]!=SHIP){
				board[randomRow][randomColumn]=SHIP;
				count=count+1;	
			}
		}			
	}
	public void printBoard(boolean withShips){
		//Numbers Header
		char letter='A';
		System.out.print(" ");
		for(int i=1;i<=NUM_COLS;i++){
			System.out.print(" "+i);
		}
			
		System.out.println();
		//Each row
		
		for (int i=0;i<NUM_ROWS;i++){
			System.out.print(letter+" ");
			for (int j=0;j<NUM_COLS;j++){
				if(!withShips){
					if(board[i][j]==SHIP){
						System.out.print(EMPTY+" ");
					}else{
						System.out.print(board[i][j]+" ");
						}
				}else{
					System.out.print(board[i][j]+" ");
				}
		
			}
			System.out.println();
			letter++;
		}
		
		
	}
	public int shootAt(char row,int col){
		int rowNumber=Character.getNumericValue(Character.toUpperCase(row))-Character.getNumericValue('A');
		if ((rowNumber<0) ||(rowNumber>NUM_ROWS-1)){
			return RESULT_ERROR;
			}
		col--;
		if((col<0) ||(col>NUM_COLS-1)){
			return RESULT_ERROR;
		}
		
			
		if(board[rowNumber][col]==SHIP){
			board[rowNumber][col]=SUNK;
			numShots++;
			numSunkShips++;
			return RESULT_SUNK;
			}
		if(board[rowNumber][col]==SUNK){
			numShots++;
			return RESULT_RESUNK;				
			}
		if(board[rowNumber][col]==EMPTY){
			board[rowNumber][col]=WATER;
			numShots++;
			return RESULT_WATER;
			}
		return RESULT_ERROR;
		
		}
	
	public int getNumShots(){
		return numShots;
		}
	public int getNumSunkShips(){
		return numSunkShips;
		}
	}
	
	
	
	
	

